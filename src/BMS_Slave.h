/*
 * File:   BMS_Parameters.h
 * Author: li
 *
 * Created on 21 december 2012, 15:04
 */

#ifndef BMS_SLAVE_PARAMETERS_H
#define	BMS_SLAVE_PARAMETERS_H

#include <p32xxxx.h>
#include <peripheral\system.h>
#include <stdio.h>
#include <plib.h>
#include "GenericTypeDefs.h"
#include "CANFunction.h"
#include "Zigbee.h"
#include "EEPROM.h"
#include "SystemParameterDefine.h"
#include "LTC6804_2.h"

#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_4, FPLLODIV = DIV_1, FWDTEN = ON
#pragma config WDTPS = PS4096                                                       // set the time out of watchdog timer = 2.048s
#pragma config POSCMOD = HS, FNOSC = PRIPLL, FPBDIV = DIV_8                         // external HS crystal clock source. FPBDIV setting of the peripheral bus = systemclock/ DIV_8
#pragma config FCANIO = ON

/***************************UNION***************************************/
typedef union {
    UINT32_VAL flag;
    struct {
        unsigned cell_over_voltage : 1;
        unsigned cell_under_voltage : 1;
        unsigned module_over_voltage : 1;
        unsigned module_under_voltage : 1;
        unsigned cell_over_temp_charge : 1;
        unsigned cell_under_temp_charge: 1;
        unsigned cell_over_temp_discharge : 1;
        unsigned cell_under_temp_discharge: 1;
        unsigned PCB_over_temp:1;
        unsigned PCB_under_temp:1;
        unsigned balancing_over_temp:1;
        unsigned charge_over_current : 1;
        unsigned discharge_over_current : 1;
        unsigned communication_skip: 1;
        unsigned cell_loose_connection : 1;
        unsigned SoC_low:1;
        unsigned : 16;
    } warning;
} WARNING_FLAG;



typedef union {
    UINT32_VAL flag;
    struct {
        unsigned cell_over_voltage : 1;
        unsigned cell_under_voltage : 1;
        unsigned module_over_voltage : 1;
        unsigned module_under_voltage : 1;
        unsigned cell_over_temp_charge : 1;
        unsigned cell_under_temp_charge : 1;
        unsigned cell_over_temp_discharge : 1;
        unsigned cell_under_temp_discharge : 1;
        unsigned PCB_over_temp:1;
        unsigned PCB_under_temp:1;
        unsigned balancing_over_temp:1;
        unsigned charge_over_current : 1;
        unsigned discharge_over_current :1;
        unsigned communication_lost :1;
        unsigned cell_loose_connection:1;
        unsigned temperature_sensor_loose_connection:1;
        unsigned memory_error:1;
        unsigned AFE_error:1;
        unsigned :14;
    } error;
} ERROR_FLAG;


typedef union {
    BYTE flag;
    struct{
        unsigned balancing_circuit_over_temp :1;
        unsigned master_communication_lost :1;
        unsigned module_voltage_warning :1; 
        unsigned module_config_error :1;
        unsigned internal_error :1;  
        unsigned : 3;
    }status;
}SYSTEM_STATUS;

typedef struct{

    UINT32_VAL systemID;                // unique system ID
    BYTE moduleID;                      // relative system module ID, 0-255
    
    //**********************Error level******************************
    UINT16_VAL OV_E;                   // cell over voltage 
    UINT16_VAL UV_E;                   // cell under voltage 
    UINT16_VAL OVM_E;                   // modulke over voltage 
    UINT16_VAL UVM_E;                  // module under voltage 
    UINT32_VAL OCC_E;                  // cell over current charge
    UINT32_VAL OCD_E;                  // cell over current discharge
    BYTE OTC_E;                         // over temperature  charge
    BYTE UTC_E;                         // under tempearture charge
    BYTE OTD_E;                         // over temperature  discharge
    BYTE UTD_E;                         // under tempearture discharge
    BYTE OTB_E;                         // over temprature balancing
    BYTE OTPCB_E;                       // over temperature PCB
    BYTE UTPCB_E;                       // under temperature pcb
    //BYTE TIME_E;                        // error recovery time
        //**********************Warning level******************************
    UINT16_VAL OV_W;                   // cell over voltage 
    UINT16_VAL UV_W;                   // cell under voltage 
    UINT16_VAL OVM_W;                   // modulke over voltage 
    UINT16_VAL UVM_W;                  // module under voltage 
    UINT32_VAL OCC_W;                  // cell over current charge
    UINT32_VAL OCD_W;                  // cell over current discharge
    BYTE OTC_W;                         // over temperature  charge
    BYTE UTC_W;                         // under tempearture charge
    BYTE OTD_W;                         // over temperature  discharge
    BYTE UTD_W;                         // under tempearture discharge
    BYTE OTB_W;                         // over temprature balancing
    BYTE OTPCB_W;                       // over temperature PCB
    BYTE UTPCB_W;                       // under temperature pcb
    //BYTE TIME_W;

    BYTE SOCL_W;                        // state of charge low warning level

}SAFETY_DATA;


typedef struct{
    UINT64 mSecCounter_old;
    UINT64 charge_mA_mS;
    UINT64 discharge_mA_mS;
    BYTE currentDirection;
    UINT32 current[12];         //mA
    UINT16 balancing_current[12];
}DSOC;

typedef struct {

    UINT32_VAL systemID;
    BYTE channelsUsed;
    UINT32_VAL BMSPowerCycle;
    UINT64_VAL powerOnTimer; // second counter for system power up
    BYTE moduleID; // moduleID in the running system
    UINT16_VAL CANBaseID;
    BYTE heartBeat;
    UINT32_VAL chargeCycle; // when DoD is more than 80% or charge tinme is continous for 200S ?? 
    UINT64_VAL miniChargeCycle;

    BYTE balancingMode;
    UINT16_VAL moduleVoltage; //  1 mV max 65V

    BYTE currentDirection;
    BYTE currentDirection_old;
    UINT32_VAL current; // mA

    UINT16_VAL voltage[12]; // use 200 offset for example if cell voltage is 3.41 V voltage= 141 range = 2V- 4.55V, and max[12], number of max[13], min[14], numberof min[15]
    UINT16_VAL maxVoltage;
    BYTE maxVoltageNr;
    UINT16_VAL minVoltage;
    BYTE minVoltageNr;
    BYTE balancingSwitch[12];
    BYTE PCBTemp;
    BYTE balancingTemp[3];
    BYTE temp[6];
    BYTE maxTemp;
    BYTE maxTempNr;
    BYTE minTemp;
    BYTE minTempNr;
    BYTE maxBalancingTemp;

    BYTE error;
    BYTE warning;
    WARNING_FLAG warningFlag;
    ERROR_FLAG errorFlag;

    BYTE masterHeartBeatSkip;
    BYTE masterCommunicationLost;
    BYTE balanceStatus[2];

} MODULE_DATA;

typedef struct {

    BYTE heartBeat_old;
    BYTE heartBeat_new;
    BYTE communication_skip;    // if the heartbeat receive from the slave is not continous
    BYTE communication_stop;    // if the heartbaet is not updated, and slave stop reaceiving message from the master
    BYTE currentDirection;
    UINT32_VAL current; // mA
    BYTE systemErrorCode;
    BYTE systemWarningCode;
    BYTE balancingMode;
    BYTE system_info_request;
    BYTE totalSlaveNumber; // byte that set the slave module into the sleep mode
    BYTE safeShutDown;
    UINT16_VAL minCellVoltage;
    UINT16_VAL maxCellVoltage;
    UINT16_VAL balanceLowLimit;
    BYTE balanceEnable[2];
    
} MASTER_DATA;


typedef union{
    BYTE byte[LOGGING_SIZE];
    struct{
        BYTE moduleID;
        BYTE channelsUsed;
        BYTE balancingTempLimit;
        //BYTE SoC[12];
        //BYTE SoH[12];
    }data;
}LOGGING_DATA;

typedef struct{
    BOOL memoryReset;
    BOOL moduleIDReset;
    BYTE moduleID;
    BYTE SoCReset;
    BYTE SoHReset;
}RESET_DATA;

//****************************************Function Prototype***************************************


void SysInit(void);

//********************system Init****************************//
void IOInit(void);
void UARTInit(void);
void SpiInit(void);
void TimerInit(void);
void AD10Init(void);
void CANInit(void);

//**********************IC Init *******************************//
void AFEInit(void);

//**********************Parameters Init***********************//
void ParametersInit(void);
void GetTemperature(void);
void GetCurrent(void);
void GetCellVoltage(void);
void GetSoC(void);
void GetSoH(void);
void SOCCalibration(void);
void OCVToSOC(void);
void CheckWarning(void);
void CheckError(void);
void SendData(void);
void SendData_CAN(void);
void SendData_RF(void);
void ReceiveData(void);
void ReceiveData_CAN(void);
void ReceiveData_RF(void);
void CellBalancing(void);
void SetStatus(BYTE status); // set status LEDs
void Wait_mS(WORD ms);
BYTE TempLookUp(WORD temp_int);
#ifdef FGT
BYTE TempLookUpFGT(WORD temp_int);
#endif
void _mon_putc(char c);
void SafeShutDown(void);        // memory access, save data safely
BOOL selfTest(void);    // system self test for memory and AFE.....


//**********WATCH DOG TIMER RESET FLAG*****************



BYTE WDTReset;


BYTE CAN1_Tx[8];
CANRxMessageBuffer* CAN1_Rx;
CAN_TX_MSG_SID Tx1Id;

WORD_VAL Lowest_Cell_Voltage;

BYTE read_Config[6];
WORD Cell_Voltage_Word[12]; // 12 cell voltage
BYTE Cell_SoC[12];
BYTE Memory_Read[LOGGING_SIZE];
BYTE Memory_Write[LOGGING_SIZE];

MODULE_DATA Module_Data; // need to be init before use it !! allocate the memory if needed
MASTER_DATA Master_Data;

SAFETY_DATA Safety_Data,Safety_Data_New;
LOGGING_DATA Logging_Data;
RESET_DATA Reset_Data;

SYSTEM_STATUS System_status;

SpiChannel Spi_LTC = SPI_CHANNEL1;
SpiChannel Spi_MEM = SPI_CHANNEL2;


BOOL CAL_LOOP_FLAG;
BOOL DATA_SEND_FLAG;
BOOL BALANCING_FLAG;
BOOL MASTER_COMMUNICATION_LOST_FLAG;
BOOL ONE_SECOND_FLAG;
BOOL CHARGE_FLAG;
BOOL SAFE_SHUT_DOWN_FLAG;
BOOL MEMORY_RESET_ALL_FLAG;
BOOL MEMORY_RESET_MODULE_ID_FLAG;
BOOL MEMORY_RESET_SOC_FLAG;
BOOL MEMORY_RESET_SOH_FLAG;




/*****************SoC*********************/
DSOC dSoC;


/****************Counters****************/
UINT64 mSecCounter;
UINT16 waitMSCounter;
UINT32 masterCommunicationLostCounter;

BYTE LTCConfig[6];
//UINT64_VAL ms_test;
//UINT32_VAL current_test;

/*****************************WARNING AND ERROR **********************/
WORD warningCellOverVoltageTimer;
WORD warningCellUnderVoltageTimer;
WORD warningModuleOverVoltageTimer;
WORD warningModuleUnderVoltageTimer;
WORD warningDischargeOverCurrentTimer;
WORD warningChargeOverCurrentTimer;
WORD warningCellOverTemperatureChargeTimer;
WORD warningCellUnderTemperatureChargeTimer;
WORD warningCellOverTemperatureDischargeTimer;
WORD warningCellUnderTemperatureDischargeTimer;
WORD warningPCBOverTemperatureTimer;
WORD warningPCBUnderTemperatureTimer;
WORD warningBalancingOverTemperatureTimer;
WORD warningBalancingUnderTemperatureTimer;
WORD warningSoCLowTimer;

WORD errorCellOverVoltageTimer;
WORD errorCellUnderVoltageTimer;
WORD errorModuleOverVoltageTimer;
WORD errorModuleUnderVoltageTimer;
WORD errorDischargeOverCurrentTimer;
WORD errorChargeOverCurrentTimer;
WORD errorCellOverTemperatureChargeTimer;
WORD errorCellUnderTemperatureChargeTimer;
WORD errorCellOverTemperatureDischargeTimer;
WORD errorCellUnderTemperatureDischargeTimer;
WORD errorPCBOverTemperatureTimer;
WORD errorPCBUnderTemperatureTimer;
WORD errorBalancingOverTemperatureTimer;
WORD errorBalancingUnderTemperatureTimer;
WORD errorBoardOverTemperatureTimer;

BYTE channelsUsedInModule;
BYTE newBalancingCircuitTempLimit;
BYTE config[6]={0x00,0x00,0x00,0x00,0x00,0x00};


/***************************Constant SOC to OCV***********************/
// based on the SOC value, get the OCV value
//




const WORD NTC_10K[165] = { 982,980,977,974,971,968,965,962,958,954,
                            951,947,942,938,934,929,924,919,914,909,
                            898,892,886,879,873,866,860,853,845,838,
                            831,823,815,807,799,791,782,773,765,756,
                            747,738,728,719,710,700,690,681,671,661,
                            651,641,631,621,611,601,591,581,571,561,
                            551,541,532,522,512,502,493,483,474,464,
                            455,446,427,428,419,410,402,393,385,376,
                            368,360,352,345,337,330,322,315,308,301,
                            294,288,281,275,268,262,256,250,245,239,
                            233,228,223,218,213,208,203,198,194,189,
                            185,181,176,172,168,164,161,157,153,150,
                            147,143,140,137,134,131,128,125,122,119,
                            117,114,111,109,107,104,102,100,98,95,
                            93,91,89,87,86,84,82,80,79,77,
                            75,74,72,71,69,68,66,65,64,62,
                            61,60,59,58,56};



#endif	/* BMS_PARAMETERS_H */

