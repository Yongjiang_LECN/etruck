/* 
 * File:   SystemParameterDefine.h
 * Author: y.li
 *
 * Created on 23 april 2014, 8:56
 */

#ifndef SYSTEMPARAMETERDEFINE_H
#define	SYSTEMPARAMETERDEFINE_H





#define SYS_FREQ                    (80000000L)
#define GetSystemClock()            (80000000ul)
#define GetPeripheralClock()        (GetSystemClock()/(1 << OSCCONbits.PBDIV))
#define GetInstructionClock()       (GetSystemClock())          (80000000L)



//******************************************ADC****************************************
#define PARAM1  ADC_MODULE_ON | ADC_FORMAT_INTG | ADC_CLK_AUTO | ADC_AUTO_SAMPLING_ON
#define PARAM2  ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_ON | ADC_SAMPLES_PER_INT_10 | ADC_ALT_BUF_ON | ADC_ALT_INPUT_OFF
#define PARAM3  ADC_CONV_CLK_PB | ADC_SAMPLE_TIME_30 |ADC_CONV_CLK_63Tcy2
#define PARAM4  ENABLE_AN0_ANA | ENABLE_AN1_ANA | ENABLE_AN2_ANA | ENABLE_AN3_ANA |ENABLE_AN4_ANA | ENABLE_AN5_ANA | ENABLE_AN8_ANA| ENABLE_AN9_ANA| ENABLE_AN10_ANA| ENABLE_AN11_ANA
#define PARAM5  SKIP_SCAN_AN6 |SKIP_SCAN_AN7 | SKIP_SCAN_AN12 | SKIP_SCAN_AN13 | SKIP_SCAN_AN14 | SKIP_SCAN_AN15


//***************DEBUG define*************************
//#define FGT
#define DEBUG_CAN
#define DEBUG_ZIGBEE
#define CAN
//#define ZIGBEE
#define FIXED_SAFETY_DATA
#define CURRENT_OVER_CAN
//#define CURRENT_AT_MODULE
//#define LTC6803
#define LTC6804

//**************** CELL information, capacity and voltage***********
#define CELL_CAPACITY_AH   10   // Ah  can be reset from the can configuration

#define BALANCING_VOLTAGE_DIFFERENCE 10 // 5mV differnence
#define BALANCING_SOC_VOLTAGE_DIFFERENCE 2 // 2mV combined voltage and soc balancing
#define BALANCING_SOC_DIFFERENCE 2  // 1% difernence one the SOC based balancing
#define BALANCING_RESISTOR 33 // 33 ohm balancing resistor

#define TEMP_OFFSET 45

#define MODULE_VOLTAGE_DIFF 3000    // maximum 3V total module voltage difference 

//************************Safety data setting************************
#define CELL_ABS_MAX_VOLTAGE 3600       // cell max voltage 4150mV
#define CELL_ABS_MIN_VOLTAGE 2300       // cell min voltage 2800mV
#define CELL_ABS_MAX_TEMP_CHARGE 90     // cell max charge temperature
#define CELL_ABS_MIN_TEMP_CHARGE 45      // cell min charge temperature
#define CELL_ABS_MAX_TEMP_DISCHARGE 95  // cell max discharge temperature, with offset 40
#define CELL_ABS_MIN_TEMP_DISCGARGE 15 // cell min discharge temperature


#define CELL_ABS_CURRENT_CHARGE     100000 // cell max charge current mA
#define CELL_ABS_CURRENT_DISCHARGE  300000 // cell max discharge current mA


#define CELL_SOC_100_VOLTAGE 3400
#define CELL_SOC_0_VOLTAGE 2500

#define CELL_LOSE_CONNECTION_VOLTAGE 1000   // if cell voltage <1000mV, means there is a lose connection


#define SAFETY_DATA_OVER_VOLTAGE_ERROR 3590         // 4.15V
#define SAFETY_DATA_UNDER_VOLTAGE_ERROR 2200        //3.00V
#define SAFETY_DATA_MODULE_OVER_VOLTAGE_ERROR 43200 // 4.1*12 = 49.2V
#define SAFETY_DATA_MODULE_UNDER_VOLTAGE_ERROR 24000 // 3.1*12 = 37.2V
#define SAFETY_DATA_OVER_CURRENT_CHARGE_ERROR   100000  // 100A
#define SAFETY_DATA_OVER_CURRENT_DISCHARGE_ERROR 300000 // 300A
#define SAFETY_DATA_OVER_TEMPERATURE_CHARGE_ERROR 90    // 40 DEGREE +45 = 85
#define SAFETY_DATA_UNDER_TEMPERATURE_CHARGE_ERROR  45  // 0 DEGREE +45 = 45
#define SAFETY_DATA_OVER_TEMPERATURE_DISCHARGE_ERROR 105 // 60 DEGREE +45 = 95
#define SAFETY_DATA_UNDER_TEMPERATURE_DISCHARGE_ERROR 25 // -20 DEGREE +45 = 25
#define SAFETY_DATA_BALANCING_OVER_TEMPERATURE_ERROR 115    // 70 DEGREE +45 = 115, IF THE BALANCING TEMPERATURE IS MORE THAN 70, SWITCH OFF THE BALANCING FUNCTION
#define SAFETY_DATA_PCB_OVER_TEMPERATURE_ERROR 115  // 70 DEGREE +45 = 115
#define SAFETY_DATA_PCB_UNDER_TEMPERATURE_ERROR 5   // -40 DEGREE +45 = 5
#define SAFETY_DATA_HEART_BEAT_LOT_ERROR 10

#define SAFETY_DATA_OVER_VOLTAGE_WARNING 3300         // 4.15V
#define SAFETY_DATA_UNDER_VOLTAGE_WARNING 2600        //3.00V
#define SAFETY_DATA_MODULE_OVER_VOLTAGE_WARNING 43000 // 4.1*12 = 49.2V
#define SAFETY_DATA_MODULE_UNDER_VOLTAGE_WARNING 25000 // 3.1*12 = 37.2V
#define SAFETY_DATA_OVER_CURRENT_CHARGE_WARNING   80000  // 100A
#define SAFETY_DATA_OVER_CURRENT_DISCHARGE_WARNING 200000 // 300A
#define SAFETY_DATA_OVER_TEMPERATURE_CHARGE_WARNING 85    // 40 DEGREE +45 = 85
#define SAFETY_DATA_UNDER_TEMPERATURE_CHARGE_WARNING  50  // 0 DEGREE +45 = 45
#define SAFETY_DATA_OVER_TEMPERATURE_DISCHARGE_WARNING 100 // 60 DEGREE +45 = 95
#define SAFETY_DATA_UNDER_TEMPERATURE_DISCHARGE_WARNING 30 // -20 DEGREE +45 = 25
#define SAFETY_DATA_BALANCING_OVER_TEMPERATURE_WARNING 110    // 70 DEGREE +45 = 115, IF THE BALANCING TEMPERATURE IS MORE THAN 70, SWITCH OFF THE BALANCING FUNCTION
#define SAFETY_DATA_PCB_OVER_TEMPERATURE_WARNING 110  // 70 DEGREE +45 = 115
#define SAFETY_DATA_PCB_UNDER_TEMPERATURE_WARNING 10   // -40 DEGREE +45 = 5
#define SAFETY_DATA_SOC_LOW  10


//**********************************LED**********************************************
#define RED_LED             LATEbits.LATE3
#define YELLOW_LED          LATEbits.LATE4
#define GREEN_LED           LATEbits.LATE5
#define BLUE_LED            LATEbits.LATE6
//*********************************SYSTEM Flag Time*********************************

#define CAL_LOOP_TIME  20          // 25ms for the measurement and calcualtion loop
#define DATA_SEND_TIME 100          // 1000ms for CAN message up date loop
#define BALANCING_TIME 5000         // 5000ms for recalculate the balancing loop
#define ONE_SECOND_TIME 1000        // one second timer

#define MASTER_COMMUNICATION_LOST_TIME 5000 //if there is no master message for 5 second, there is a go to sleep mode....

#define ERROR_TIME      4000             // after 2000ms if the error condition still true, then set the error flag
#define WARNING_TIME    10000             // after 5000ms if the warning condition still true, then set the warning flag

#define CHARGE_TIME     60000            // if current is continously charge the module for 60s, then charge flag ==1;


#define SYSTEM_MEMORY_RESET_MODULE_ID_CAN_ID 0x700
#define SYSTEM_MODULE_CONFIG_ID_CAN_ID 0X701




#define BALANCE_TEMP_LOW_LIMIT  130 // 130-45 = 85
#define MAXIMUM_MODULE_ID 40    // maximum 40 modules in system
#define MAXIMUM_CHANNELS_IN_USE  12 // maximum 12 channels 
#define MASTER_TIME_OUT        20000 //20s 





#define CHARGE_CURRENT   1
#define DISCHARGE_CURRENT 0

//*****************************ZIGBEE*******************************************
#define ZIGBEE_BD 38400

//*****************************CAN**********************************************
#define CAN_BUS_SPEED   500000
#define CAN1_BRPVAL	0x07			/* CAN speed select - 0x7 for 500kbps, 0xF for 250Kbps, 0x3 for 1Mbps 		*/
#define CAN2_BRPVAL     0x07			/* Use same speed for both CAN modules. Time quanta per bit is set to 10.	*/
								/* See function CAN1Init() and CAN2Init().*/

//************************ MEMORY ADDRESS FOR DATA*****************************
//************************ 64*128Bytes 
#define MEMORY_SYSTEM_INFO   0x0000


#define MEMORY_MODULE_DATA_INFO     0x080

#define MEMORY_SAFETY_LEVEL_INFO    0x0200

#define MEMORY_LOGGING_STATISIC     0x3000
#define MEMORY_DATA_LOGGING         0x3080
#define LOGGING_SIZE                30



#define CURRENT_DATA_INTERVAL   50      // 50ms for current value

#endif	/* SYSTEMPARAMETERDEFINE_H */

