/*
 * File:   EEPROM_SPI.h
 * Author: y.li
 *
 * Created on 13 november 2013, 11:10
 */




/*****************************************************************************
 * EEPROM memory data location
 *
 * max page = 512
 * data per page = 128 byte
 *
 *
 *
 *
 *
 *
 *
 *
 *****************************************************************************
 */

#ifndef EEPROM_H
#define	EEPROM_H


#include "GenericTypeDefs.h"
#include <p32xxxx.h>
#include  <plib.h>
#include "SystemParameterDefine.h"


#define EEPROM_CS  LATGbits.LATG9


#define WREN        0b00000110          // WREN command
#define WRDI        0b00000100          // WRDI command
#define WRITE       0b00000010             // page programing
#define READ        0b00000011          // READ command
#define WRSR        0b00000001          // WRSR command
#define RDSR        0b00000101          // RDSR command
#define CERASE      0b11000111          // chip erase command
#define RDID        0b10101011          // release from deap power down and aread electronics signature
#define DPD         0b10111001          // deap power down mode

#define EEPROM 32                      // 512Kbit
#define PAGESIZE 32                    // 128-byte page


BYTE SPI_RW(BYTE data);
BYTE ReadStatus(void);
BYTE WriteStatus(BYTE status);

//BYTE EWriteByte(WORD addr,BYTE data);
//BYTE ReadByte(WORD addr,BYTE data);
BYTE getCRC(const BYTE* data, const int width);
BYTE WriteArray(WORD addr, BYTE* data, WORD width);
BYTE ReadArray(WORD addr, BYTE* data,WORD width);
BYTE WritePage(WORD addr, BYTE* data, WORD width);
BYTE ReadPage(WORD addr, BYTE* data,WORD width);
BYTE DeapPowerDown(void);
BYTE ReleaseFromDeapPowerDown(void);

BYTE BulkErase(void);





#endif
