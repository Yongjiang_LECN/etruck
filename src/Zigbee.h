/* 
 * File:   Zigbee.h
 * Author: y.li
 *
 * Created on 31 maart 2014, 12:15
 */

#ifndef ZIGBEE_H
#define	ZIGBEE_H

#include "GenericTypeDefs.h"
#include <p32xxxx.h>
#include <plib.h>
#include "SystemParameterDefine.h"



#define ZIGBEE_SLEEP PORTEbits.RE7

void ZigbeeInit(void);
void Zigbee_changeID(void);
WORD getPANID(void);
WORD getNetwordID(void);
void ZigbeeTxMsg_Master(const BYTE* data,BYTE length);
void ZigbeeTxMsg_Broadcasting(const BYTE* data,BYTE length);
void ZigbeeRxMsg(BYTE* data);




#endif	/* ZIGBEE_H */

