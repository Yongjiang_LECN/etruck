
/**********************************************************************
 * � Trineuron, a division of Emrol
 ********************************************************************/





#include "BMS_Slave.h"

/*** start of the code
 * Init and configuration bit setting
 */




int main(void) {

    BOOL balance_toogle = TRUE;
    SysInit(); // system and clock init, run at 80MHz
    ParametersInit();
    //enable the system interrupt
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();


    EnableWDT();


    while (1) {

        //        //********************CALCULATION_LOOP**********************************
        if (CAL_LOOP_FLAG == TRUE) {
            CAL_LOOP_FLAG = FALSE;

            GetTemperature();
            GetCellVoltage();


            RED_LED ^= 1;
            YELLOW_LED = 1;


            ClearWDT();
        }
        //
        //        //**********************SEND_DATA_LOOP**********************************
        if (DATA_SEND_FLAG == TRUE) {
            DATA_SEND_FLAG = FALSE;
            SendData();
            ClearWDT();
        }
        //**********************BALANCING_LOOP**********************************
        if (BALANCING_FLAG == TRUE) {
            BALANCING_FLAG = FALSE;
            CellBalancing();
            Module_Data.balanceStatus[0] = config[4];
            Module_Data.balanceStatus[1] = config[5];
        }

        //*************************ReceiveData**********************************
        ReceiveData();

    }

}

/**
 *    sysInit
 * set the system frequency at 80Mhz
 * enable the Interrupt for the
 */

void SysInit(void) {

    // set the system frequency @ 80Mhz
    SYSTEMConfig(SYS_FREQ, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
    DDPCONbits.JTAGEN = 0; // disable the JTAG function

    DisableWDT();
    ClearEventWDT();
    IOInit(); // Init for all the IO, Digital IO and Analog IO
    CANInit();
    SpiInit(); // init the SPI1 for LTC and SPI2 for EEPROM
    TimerInit(); /* Function is defined in TimerFunctions.c    */
    AD10Init(); // init for analog input , temperature sensors
    //ZigbeeInit(); // UART1 and send Baudrate 38400bps
    //AFEInit();
    LTC6804_initialize();
    LTC6804_wrcfg(1, config);
    LTC6804_adcv();
    selfTest();
}

/**
 * SysInit
 *
 */
void IOInit(void) {

    /***********Clear PORT latch default value***********************************/
    LATA = 0;
    LATB = 0;
    LATC = 0;
    LATD = 0;
    LATE = 0;
    LATF = 0;

    TRISDbits.TRISD11 = 0; //   RD11 CSBI for LTC6803
    TRISDbits.TRISD7 = 1; //    RD7 WDTB input for LTC6803

    TRISGbits.TRISG9 = 0; //    RG9 SPI EEPROM CE
    TRISGbits.TRISG12 = 0; //    RG12 SPI EEPROM WP

    TRISFbits.TRISF8 = 0; // Zigbee UART1 TX
    TRISFbits.TRISF2 = 1; // zigbee UART1 RX
    TRISEbits.TRISE7 = 0; // zigbee sleep control

    TRISBbits.TRISB0 = 1; // Analog input
    TRISBbits.TRISB1 = 1;
    TRISBbits.TRISB2 = 1;
    TRISBbits.TRISB3 = 1;
    TRISBbits.TRISB4 = 1;
    TRISBbits.TRISB5 = 1;

    TRISBbits.TRISB8 = 1;
    TRISBbits.TRISB9 = 1;
    TRISBbits.TRISB10 = 1;
    TRISBbits.TRISB11 = 1;

    TRISEbits.TRISE3 = 0; // Green LED output
    TRISEbits.TRISE4 = 0; // RED LED output
    TRISEbits.TRISE5 = 0; // Yellow LED output
    TRISEbits.TRISE6 = 0; // BLUE LED output

}

/**             SpiInit
 *              Open Spi channel
 *              Spi channel 1 for LTC6803
 *              Spi channel 2 for nRF24L01P
 * @param c
 */
void SpiInit(void) {

    SpiOpenFlags oFlags = SPICON_MSTEN | SPICON_CKE | SPICON_ON; // SPI open mode
    SpiChnOpen(Spi_LTC, oFlags, 100); // channel 1, divide fpb by 40, configure the I/O ports. Not using SS in this example 1Mhz
    SpiChnOpen(Spi_MEM, oFlags, 100); // channel 2, divide fpb by 40, configure the I/O ports. Not using SS in this example

}

void TimerInit(void) {

    //***********************TIMER1 1ms interrupt******************************
    /* This function will intialize the Timer 1
     * for basic timer operation. It will enable
     * timer interrupt. */
    T1CON = 0x0; //* Basic Timer Operation
    PR1 = 10000; //* 1ms
    IFS0CLR = 0x10; //* Clear interrupt flag and enable
    IEC0SET = 0x10; //* timer 1 interrupt.
    IPC1bits.T1IP = 7; //* Timer 1 priority is 4
    mSecCounter = 0; //* and the millisecond counter.
    T1CONSET = 0x8000; // start timer
    //
}

/**
 * CANInit(void)
 * init of the CAN bus, use the AC2RX and AC2TX
 */
void CANInit(void) {

    // CAN1Init(void) and CAN2Init(void) are definde in the header file of CANFunctions
    // BMS slave use the AC2TX and AC2RX for the CAN bus communication
    // Enable the CAN channel with speed of 500 Kbps
    // CAN1Init();
    CAN1Init();

}

/**
 *  LTC6803Init
 * Init the LTC6803 with SPI channel 1
 *
 * need to read the config register to make sure that the LTC6803 is in the normal operation mode
 * make sure that there is a feedback system or signal to indicate the operation of the LTC6803
 *
 */
void AFEInit(void) {



}

/**
 * Init the parameters
 */
void ParametersInit(void) {

    int i;
    BYTE memoryError;

    mSecCounter = 0; // mS counter, also use as the power up timer! 2^64, can never be counted !!!

    WDTReset = 0x00;


    /**********CAN data***********************************************************/
    for (i = 0; i < 8; i++) {
        CAN1_Tx[i] = 0;
    }
    CAN1_Rx = NULL;
    Tx1Id.SID = 0;

    /************memory data*************************************************/

    Reset_Data.memoryReset = FALSE;

    /*****************FLAG********************************************************/

    CAL_LOOP_FLAG = FALSE;
    DATA_SEND_FLAG = FALSE;
    BALANCING_FLAG = FALSE;
    MASTER_COMMUNICATION_LOST_FLAG = FALSE;
    ONE_SECOND_FLAG = FALSE;
    CHARGE_FLAG = FALSE;
    SAFE_SHUT_DOWN_FLAG = FALSE;
    MEMORY_RESET_ALL_FLAG = FALSE;
    MEMORY_RESET_MODULE_ID_FLAG = FALSE;
    MEMORY_RESET_SOC_FLAG = FALSE;
    MEMORY_RESET_SOH_FLAG = FALSE;
    /*****************ModuleData**************************************************/

    System_status.flag = 0;
    memoryError = ReadPage(MEMORY_SYSTEM_INFO, Memory_Read, LOGGING_SIZE);
    if (memoryError == 0)
        System_status.status.internal_error = 0;
    else
        System_status.status.internal_error = 1;

    for (i = 0; i < LOGGING_SIZE; i++) {
        Logging_Data.byte[i] = Memory_Read[i];
    }

    Module_Data.moduleID = Logging_Data.data.moduleID;
    Module_Data.channelsUsed = Logging_Data.data.channelsUsed;
    Module_Data.maxBalancingTemp = Logging_Data.data.balancingTempLimit;


    if (Module_Data.moduleID > MAXIMUM_MODULE_ID) {
        Module_Data.moduleID = 0;
        System_status.status.module_config_error = 1;
    }
    if (Module_Data.channelsUsed > MAXIMUM_CHANNELS_IN_USE) {
        Module_Data.channelsUsed = MAXIMUM_CHANNELS_IN_USE;
        System_status.status.module_config_error = 1;
    }
    if (Module_Data.maxBalancingTemp < BALANCE_TEMP_LOW_LIMIT) {
        Module_Data.maxBalancingTemp = BALANCE_TEMP_LOW_LIMIT;
        System_status.status.module_config_error = 1;
    }



    Module_Data.powerOnTimer.Val = 0;
    Module_Data.heartBeat = 0x00;
    Module_Data.CANBaseID.Val = 0x100 + 32 * Module_Data.moduleID;
    Module_Data.moduleVoltage.Val = 0;

    Module_Data.maxVoltage.Val = 0;
    Module_Data.maxVoltageNr = 0;
    Module_Data.minVoltage.Val = 0xFFFF;
    Module_Data.minVoltageNr = 0;

    Module_Data.PCBTemp = 0;
    Module_Data.balancingTemp[0] = 0;
    Module_Data.balancingTemp[1] = 0;
    Module_Data.balancingTemp[2] = 0;

    for (i = 0; i < 12; i++) {
        Module_Data.voltage[i].Val = 0xFFFF;
        Module_Data.balancingSwitch[i] = 0;
    }
    for (i = 0; i < 6; i++)
        Module_Data.temp[i] = 0;

    Module_Data.maxTemp = 0;
    Module_Data.maxTempNr = 0;
    Module_Data.minTemp = 0;
    Module_Data.minTempNr = 0;
    Module_Data.error = 0;
    Module_Data.warning = 0;
    Module_Data.errorFlag.flag.Val = 0;
    Module_Data.warningFlag.flag.Val = 0;
    Module_Data.masterHeartBeatSkip = 0;
    Module_Data.masterCommunicationLost = 0;



    /****************Master_Data*********************/
    Lowest_Cell_Voltage.Val = 0xFFFF;
    Master_Data.heartBeat_new = Master_Data.heartBeat_old + 1;
    Master_Data.currentDirection = 0;
    Master_Data.current.Val = 0;
    Master_Data.balancingMode = 0;
    Master_Data.safeShutDown = 0;
    Master_Data.minCellVoltage = Module_Data.minVoltage;
    Master_Data.maxCellVoltage = Module_Data.maxVoltage;
    Master_Data.balanceLowLimit.Val = 0xFFFF;

}

/**
 *  AD10Init(void)
 * @param c
 */
void AD10Init(void) {

    // configure and enable the ADC
    CloseADC10(); // ensure the ADC is off before setting the configuration
    SetChanADC10(ADC_CH0_NEG_SAMPLEA_NVREF); //
    OpenADC10(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5); // configure ADC using the parameters defined above
    IEC1bits.AD1IE = 0; // desable the ADC interrupt
    EnableADC10(); // Enable the ADC

}

BOOL selfTest(void) {

    return TRUE;
}

/**
 *    Get Temperature sensor data  the 6 individual data and the max and the min sensor data
 * @param temp
 */
void GetTemperature(void) {

    BYTE temp_max = 0x00;
    BYTE temp_min = 0xff;
    BYTE temp_max_nr = 0;
    BYTE temp_min_nr = 0;

    BYTE i;

    if(TempLookUp(ReadADC10(0))<3)
       Module_Data.PCBTemp = 0; 
    else
       Module_Data.PCBTemp = TempLookUp(ReadADC10(0))-3;

    Module_Data.balancingTemp[1] = TempLookUp(ReadADC10(1));
    Module_Data.balancingTemp[2] = TempLookUp(ReadADC10(2));

    if (Module_Data.balancingTemp[1] > Module_Data.balancingTemp[2])
        Module_Data.balancingTemp[0] = Module_Data.balancingTemp[1];
    else
        Module_Data.balancingTemp[0] = Module_Data.balancingTemp[2];

    for (i = 0; i < 6; i++) {
        Module_Data.temp[i] = TempLookUp(ReadADC10(i + 4));
        if (Module_Data.temp[i] > temp_max) {
            temp_max = Module_Data.temp[i];
            temp_max_nr = i;
        }
        if (Module_Data.temp[i] < temp_min) {
            temp_min = Module_Data.temp[i];
            temp_min_nr = i;
        }
    }

#ifdef FGT
    for (i = 0; i < 6; i++) {
        Module_Data.temp[i] = TempLookUpFGT(ReadADC10(i + 4)) + 45;
        if (i > 3) {
            Module_Data.temp[i] = Module_Data.temp[i - 3];
        }
        if (Module_Data.temp[i] > temp_max) {
            temp_max = Module_Data.temp[i];
            temp_max_nr = i;
        }
        if (Module_Data.temp[i] < temp_min) {
            temp_min = Module_Data.temp[i];
            temp_min_nr = i;
        }
    }
#endif


    Module_Data.maxTemp = temp_max;
    Module_Data.minTemp = temp_min;
    Module_Data.maxTempNr = temp_max_nr;
    Module_Data.minTempNr = temp_min_nr;

}

/**
 * GetCurrent
 * read the current value and the current direction from the INA226
 *
 */
void GetCurrent(void) {


}

/**
 *
 * @param voltage_byte
 */
void GetCellVoltage(void) {

    BYTE i;
    BYTE crc_flag;
    WORD module_voltage_ADC = 0;
    WORD module_voltage = 0;
    WORD max_voltage = 0;
    WORD min_voltage = 0xffff;
    BYTE max_voltage_nr = 0;
    BYTE min_voltage_nr = 0;
    UINT16 cVoltage[12];

    LTC6804_rdcv(0, cVoltage);

    // convert cell voltage from 100uV to 1mV resolution 
    for (i = 0; i < Module_Data.channelsUsed; i++)
        Module_Data.voltage[i].Val = cVoltage[i] / 10;

    for (i = 0; i < Module_Data.channelsUsed; i++) {
        if (Module_Data.voltage[i].Val > max_voltage) {
            max_voltage = Module_Data.voltage[i].Val;
            max_voltage_nr = i;
        }
        if (Module_Data.voltage[i].Val < min_voltage) {
            min_voltage = Module_Data.voltage[i].Val;
            min_voltage_nr = i;
        }
        module_voltage = module_voltage + Module_Data.voltage[i].Val;
    }
    module_voltage_ADC = (ReadADC10(3)*3300) >> 5;
    
    if (abs(module_voltage - module_voltage_ADC) > MODULE_VOLTAGE_DIFF)
        System_status.status.module_voltage_warning = 1;
    else
        System_status.status.module_voltage_warning = 0;


    Module_Data.maxVoltage.Val = max_voltage;
    Module_Data.minVoltage.Val = min_voltage;
    Module_Data.maxVoltageNr = max_voltage_nr;
    Module_Data.minVoltageNr = min_voltage_nr;
    Module_Data.moduleVoltage.Val = ((module_voltage_ADC + 5) / 10);
    LTC6804_wrcfg(1, config);
    LTC6804_adcv();
}

/**
 *
 * @param SOC
 * intergrate the current for SoC
 */
void GetSoC(void) {

}


// get the SOC from cell voltage, call this function when there is no current flow
// used as self test function
// do it before the contactor is closed !! no current is flowing through the shunt current sensor and no voltage drop at the shunt amplifier

void GetSoH(void) {




}

void OCVToSOC(void) {


}

/**
 * SOC Calibration
 * call it for init the start value of the SOC
 */

void SOCCalibration(void) {


}

/**
 *
 * @param data
 * @return BOOL yes= send data success no= send data not scuess
 */
void SendData(void) {

    GREEN_LED ^= 1;
    Module_Data.heartBeat++;

    SendData_CAN();


#ifdef ZIGBEE
    SendData_RF();
#endif

}

/**
 *  if CANSelect ==0x01
 *  Module data is send via CAN
 */
void SendData_CAN(void) {

    if (masterCommunicationLostCounter >= MASTER_TIME_OUT){
        System_status.status.master_communication_lost = 1;
        masterCommunicationLostCounter = MASTER_TIME_OUT;
    }
    else
        System_status.status.master_communication_lost = 0;
    
    Tx1Id.SID = Module_Data.CANBaseID.Val + 1;
    CAN1_Tx[0] = Module_Data.heartBeat;
    CAN1_Tx[1] = Module_Data.moduleVoltage.byte.HB;
    CAN1_Tx[2] = Module_Data.moduleVoltage.byte.LB;
    CAN1_Tx[3] = Master_Data.balanceLowLimit.byte.HB;
    CAN1_Tx[4] = Master_Data.balanceLowLimit.byte.LB;
    CAN1_Tx[5] = Module_Data.balanceStatus[1];
    CAN1_Tx[6] = Module_Data.balanceStatus[0];
    CAN1_Tx[7] = System_status.flag;

    CAN1TxMsg(Tx1Id, CAN1_Tx);


    Tx1Id.SID = Module_Data.CANBaseID.Val + 2;
    CAN1_Tx[0] = Module_Data.voltage[0].byte.HB;
    CAN1_Tx[1] = Module_Data.voltage[0].byte.LB;
    CAN1_Tx[2] = Module_Data.voltage[1].byte.HB;
    CAN1_Tx[3] = Module_Data.voltage[1].byte.LB;
    CAN1_Tx[4] = Module_Data.voltage[2].byte.HB;
    CAN1_Tx[5] = Module_Data.voltage[2].byte.LB;
    CAN1_Tx[6] = Module_Data.voltage[3].byte.HB;
    CAN1_Tx[7] = Module_Data.voltage[3].byte.LB;

    CAN1TxMsg(Tx1Id, CAN1_Tx);

    Tx1Id.SID = Module_Data.CANBaseID.Val + 3;
    CAN1_Tx[0] = Module_Data.voltage[4].byte.HB;
    CAN1_Tx[1] = Module_Data.voltage[4].byte.LB;
    CAN1_Tx[2] = Module_Data.voltage[5].byte.HB;
    CAN1_Tx[3] = Module_Data.voltage[5].byte.LB;
    CAN1_Tx[4] = Module_Data.voltage[6].byte.HB;
    CAN1_Tx[5] = Module_Data.voltage[6].byte.LB;
    CAN1_Tx[6] = Module_Data.voltage[7].byte.HB;
    CAN1_Tx[7] = Module_Data.voltage[7].byte.LB;

    CAN1TxMsg(Tx1Id, CAN1_Tx);

    Tx1Id.SID = Module_Data.CANBaseID.Val + 4;
    CAN1_Tx[0] = Module_Data.voltage[8].byte.HB;
    CAN1_Tx[1] = Module_Data.voltage[8].byte.LB;
    CAN1_Tx[2] = Module_Data.voltage[9].byte.HB;
    CAN1_Tx[3] = Module_Data.voltage[9].byte.LB;
    CAN1_Tx[4] = Module_Data.voltage[10].byte.HB;
    CAN1_Tx[5] = Module_Data.voltage[10].byte.LB;
    CAN1_Tx[6] = Module_Data.voltage[11].byte.HB;
    CAN1_Tx[7] = Module_Data.voltage[11].byte.LB;

    CAN1TxMsg(Tx1Id, CAN1_Tx);

    Tx1Id.SID = Module_Data.CANBaseID.Val + 5;
    CAN1_Tx[0] = Module_Data.PCBTemp;
    CAN1_Tx[1] = Module_Data.balancingTemp[0];
    CAN1_Tx[2] = Module_Data.temp[0];
    CAN1_Tx[3] = Module_Data.temp[1];
    CAN1_Tx[4] = Module_Data.temp[2];
    CAN1_Tx[5] = Module_Data.temp[3];
    CAN1_Tx[6] = Module_Data.temp[4];
    CAN1_Tx[7] = Module_Data.temp[5];

    CAN1TxMsg(Tx1Id, CAN1_Tx);

    Tx1Id.SID = Module_Data.CANBaseID.Val + 6;
    CAN1_Tx[0] = Module_Data.maxVoltageNr + 1;
    CAN1_Tx[1] = Module_Data.maxVoltage.byte.HB;
    CAN1_Tx[2] = Module_Data.maxVoltage.byte.LB;
    CAN1_Tx[3] = Module_Data.minVoltageNr + 1;
    CAN1_Tx[4] = Module_Data.minVoltage.byte.HB;
    CAN1_Tx[5] = Module_Data.minVoltage.byte.LB;
    CAN1_Tx[6] = 0;
    CAN1_Tx[7] = 0;

    CAN1TxMsg(Tx1Id, CAN1_Tx);

    Tx1Id.SID = Module_Data.CANBaseID.Val + 7;
    CAN1_Tx[0] = Module_Data.maxTempNr + 1;
    CAN1_Tx[1] = Module_Data.maxTemp;
    CAN1_Tx[2] = Module_Data.minTempNr + 1;
    CAN1_Tx[3] = Module_Data.minTemp;
    CAN1_Tx[4] = 0;
    CAN1_Tx[5] = Module_Data.moduleID;
    CAN1_Tx[6] = Module_Data.maxBalancingTemp;
    CAN1_Tx[7] = Module_Data.channelsUsed;

    CAN1TxMsg(Tx1Id, CAN1_Tx);


}

/**
 * send data via wireless
 */
void SendData_RF(void) {


}

/**
 * receiveData, if
 */
void ReceiveData(void) {


    ReceiveData_CAN();


}

/**
 * ReceiveData from CAN
 *
 */
void ReceiveData_CAN(void) {

    CAN1_Rx = (CANRxMessageBuffer *) CAN1RxMsg();
    if (CAN1_Rx != NULL) {
        BLUE_LED = 1;
        if (CAN1_Rx->msgSID.SID == Module_Data.CANBaseID.Val) {

            MASTER_COMMUNICATION_LOST_FLAG = FALSE;
            masterCommunicationLostCounter = 0;
            Master_Data.heartBeat_new = CAN1_Rx->data[0];
            Master_Data.balanceLowLimit.byte.HB = CAN1_Rx->data[1];
            Master_Data.balanceLowLimit.byte.LB = CAN1_Rx->data[2];
            Master_Data.balanceEnable[0] = CAN1_Rx->data[3];
            Master_Data.balanceEnable[1] = CAN1_Rx->data[4];
            if (Master_Data.heartBeat_new == 0x00 && Master_Data.heartBeat_old == 0xFF)
                Module_Data.masterHeartBeatSkip = 0;
            else
                Module_Data.masterHeartBeatSkip = Master_Data.heartBeat_new - Master_Data.heartBeat_old - 1;
            Master_Data.heartBeat_old = Master_Data.heartBeat_new;
            
            
        }//*************************SYSTEM CONFIG CAN MESSAGE*******************

        else if (CAN1_Rx->msgSID.SID == SYSTEM_MEMORY_RESET_MODULE_ID_CAN_ID) {
            Reset_Data.moduleIDReset = CAN1_Rx->data[0];
            Reset_Data.moduleID = CAN1_Rx ->data[1];
            Logging_Data.data.channelsUsed = CAN1_Rx->data[2];
            if ((Reset_Data.moduleIDReset == 1) && ((Reset_Data.moduleID != Module_Data.moduleID)||(Module_Data.channelsUsed != Logging_Data.data.channelsUsed))) {
                if (MEMORY_RESET_MODULE_ID_FLAG == FALSE) {
                    MEMORY_RESET_MODULE_ID_FLAG = TRUE;
                    Logging_Data.data.moduleID = Reset_Data.moduleID;
                    WritePage(MEMORY_SYSTEM_INFO, Logging_Data.byte, LOGGING_SIZE);
                    SoftReset();
                }
            }
            else
                Logging_Data.data.channelsUsed = Module_Data.channelsUsed;   
        }
        else if (CAN1_Rx->msgSID.SID == SYSTEM_MODULE_CONFIG_ID_CAN_ID) {
            if(CAN1_Rx->data[1] !=Module_Data.maxBalancingTemp){
                Logging_Data.data.balancingTempLimit = CAN1_Rx->data[0];
                WritePage(MEMORY_SYSTEM_INFO, Logging_Data.byte, LOGGING_SIZE);
                SoftReset();
            }
        }
        BLUE_LED = 0;
    }


}

/**
 * receive data from master via RF
 */
void ReceiveData_RF(void) {

}

/**
 *  Check all the warning for the BMS slaves
 */
void CheckWarning(void) {


}

/**
 *  check all the error flag of the BMS slave
 */
void CheckError(void) {



}

/**
 *  balancing the cells during the charging phase, make sure that all the cells are within the cell balancing gape !
 *
 */
void CellBalancing(void) {

    int i;
    WORD minVoltage;
    BYTE discharge;
    BYTE_BITS config1;
    BYTE_BITS config2;

    BYTE minSoC;

    BYTE LTCConfigWrite[6] = {0x00, 0, 0, 0, 0x00, 0x00};
    config1.Val = 0x00;
    config2.Val = 0x00;
    minVoltage = Lowest_Cell_Voltage.Val;
    for (i = 0; i < 12; i++) {
        discharge = (Module_Data.voltage[i].Val > Master_Data.balanceLowLimit.Val);
        if (discharge) {
            Module_Data.balancingSwitch[i] = 1;
            switch (i) {
                case 0:
                    config1.bits.b0 = 1;
                    break;
                case 1:
                    config1.bits.b1 = 1;
                    break;
                case 2:
                    config1.bits.b2 = 1;
                    break;
                case 3:
                    config1.bits.b3 = 1;
                    break;
                case 4:
                    config1.bits.b4 = 1;
                    break;
                case 5:
                    config1.bits.b5 = 1;
                    break;
                case 6:
                    config1.bits.b6 = 1;
                    break;
                case 7:
                    config1.bits.b7 = 1;
                    break;
                case 8:
                    config2.bits.b0 = 1;
                    break;
                case 9:
                    config2.bits.b1 = 1;
                    break;
                case 10:
                    config2.bits.b2 = 1;
                    break;
                case 11:
                    config2.bits.b3 = 1;
                    break;
                default:
                    break;
            }
        }
        else
            Module_Data.balancingSwitch[i] = 0;
    }

    // temperature limit

    if (Module_Data.balancingTemp[0] > Module_Data.maxBalancingTemp)
        System_status.status.balancing_circuit_over_temp = 1;
    else 
        System_status.status.balancing_circuit_over_temp = 0;
    
    if (System_status.status.balancing_circuit_over_temp|System_status.status.master_communication_lost) {
        config[4] = 0x00;
        config[5] = 0x00;
    } else {
        config[4] = config1.Val & Master_Data.balanceEnable[1];
        config[5] = config2.Val & Master_Data.balanceEnable[0]&0x0F;
    }

    LTC6804_wrcfg(1, config);
    Master_Data.minCellVoltage.Val = Module_Data.minVoltage.Val;
    Lowest_Cell_Voltage.Val = Module_Data.minVoltage.Val;
}

/**
 *
 * @param temp_int
 * @return   BYTE temp with offset of 40
 */
BYTE TempLookUp(WORD temp_int) {

    BYTE i;
    BYTE j;
    if (temp_int > NTC_10K[0])
        return 0;
    else if (temp_int < NTC_10K[164])
        return 165;
    else {
        for (i = 0; i < 165; i++) {
            if ((temp_int <= NTC_10K[i]) && (temp_int > NTC_10K[i + 1])){
                j = (BYTE)((unsigned int)((i*3+91)>>2));
                return  j;
            }
        }
    }
}
#ifdef FGT

BYTE TempLookUpFGT(WORD temp_int) {
    BYTE temp_byte;
    temp_byte = (BYTE) (temp_int >> 3);
    if (temp_byte > 121) // in case the temp sensor is not connected !
        temp_byte = 121;
    temp_byte = 121 - temp_byte;
    return temp_byte;
}
#endif

void SafeShutDown(void) {





}

/**          _mon_putc
 * use the printf for the usart port
 * @param c
 */
void _mon_putc(char c) {
    while (U1STAbits.UTXBF);
    U1TXREG = c;
}

void Wait_mS(WORD ms) {

    waitMSCounter = 0;
    while (waitMSCounter > ms);
}

void __attribute__((vector(4), interrupt(ipl7), nomips16)) Timer1InterruptHandler(void) {
    /* This is the Timer 1 ISR */

    IFS0CLR = 0x10; /* Clear the Interrupt Flag	*/

    mSecCounter++; /* Increment millisecond counter.	*/
    masterCommunicationLostCounter++;
    waitMSCounter++;
    Module_Data.powerOnTimer.Val++;

    if (mSecCounter % CAL_LOOP_TIME == 0)
        CAL_LOOP_FLAG = TRUE;
    if (mSecCounter % DATA_SEND_TIME == 0)
        DATA_SEND_FLAG = TRUE;
    if (mSecCounter % BALANCING_TIME == 0)
        BALANCING_FLAG = TRUE;
    if (mSecCounter % ONE_SECOND_TIME == 0)
        ONE_SECOND_FLAG = TRUE;

}