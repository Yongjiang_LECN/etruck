
/*------------------------------------------------------------------------------
 * File:    EEPROM_SPI.c
 * Author   y.li@trineuron.com  (c) CopyRight Trineuron - a division of Emrol
 * Trineuron    Industrieweg 15,B-2390,Malle,Belgium
 *
 *
 * *****************************************************************************
 * File Description
 *
 * Change History:
 * Rev      Date        Description
 * 1.0      6/11/13     base software structure (no timeout function for write eeprom fail!!)
 *
 *
 *
 *------------------------------------------------------------------------------
 */


#include "EEPROM.h"

void DelayUs_SPI(WORD us);
void DelayMs_SPI(WORD ms);

BYTE SPI_RW(BYTE data) {

    SPI2BUF = data;
    while (!SpiChnDataRdy(SPI_CHANNEL2)); // Wait until the receive buffer is full
    return SPI2BUF;
}

BYTE ReadStatus(void) {

    BYTE status;
    EEPROM_CS = 0;
    SPI_RW(RDSR);
    status = SPI_RW(0);
    EEPROM_CS = 1;
    return status;
}

BYTE WriteStatus(BYTE status) {

    EEPROM_CS = 0;
    SPI_RW(WRSR);
    status = SPI_RW(status);
    EEPROM_CS = 1;

    DelayMs_SPI(1);
    if (status == ReadStatus())
        return 0;
    else
        return 1;
}


// main will

BYTE WritePage(WORD addr, BYTE* data, WORD width) {

    BYTE_VAL statusBits;
    WORD i;
    WORD_VAL address;
    BYTE crc;


    crc = getCRC(data, width);
    address.Val = addr;

    EEPROM_CS = 0;
    SPI_RW(WREN);
    EEPROM_CS = 1;

    do {
        statusBits.Val = ReadStatus();
    } while (statusBits.bits.b1 == 0); // check if the WREN is set then start wirte to the eeprom memory

    EEPROM_CS = 0;
    SPI_RW(WRITE);
    SPI_RW(address.v[1]);
    SPI_RW(address.v[0]);

    for (i = 0; i < width; i++) {
        SPI_RW(data[i]);
    }
    SPI_RW(crc); // write the CRC value for the array
    EEPROM_CS = 1;

    DelayMs_SPI(10);
    EEPROM_CS = 0;
    SPI_RW(WRDI);
    EEPROM_CS = 1;

    do {
        statusBits.Val = ReadStatus();
    } while (statusBits.bits.b0 == 1);

    return 0;
}

BYTE ReadPage(WORD addr, BYTE* data, WORD width) {

    WORD i;
    WORD_VAL address;
    BYTE crc_read, crc_cal;

    address.Val = addr;
    EEPROM_CS = 0;
    SPI_RW(READ);
    SPI_RW(address.v[1]);
    SPI_RW(address.v[0]);

    for (i = 0; i < width; i++) {
        data[i] = SPI_RW(0);
    }
    crc_read = SPI_RW(0);
    EEPROM_CS = 1;

    crc_cal = getCRC(data, width);
    if (crc_cal == crc_read)
        return 0;
    else
        return 1;
}

BYTE WriteArray(WORD addr, BYTE* data, WORD width) {

  

    return WritePage(addr, data, width);

}

BYTE ReadArray(WORD addr, BYTE* data, WORD width) {


    return ReadPage(addr, data, width);
}

BYTE BulkErase(void) {

    BYTE_VAL statusBits;

    EEPROM_CS = 0;
    SPI_RW(WREN);
    EEPROM_CS = 1;

    do {
        DelayMs_SPI(1);
        statusBits.Val = ReadStatus();
    } while (statusBits.bits.b1 == 0); //

    EEPROM_CS = 0;
    SPI_RW(CERASE);
    EEPROM_CS = 1;

    do {
        DelayMs_SPI(1);
        statusBits.Val = ReadStatus();
    } while (statusBits.bits.b0 == 1); // until write in process bit is 0 means write is finished !!

    return 0;
}

BYTE DeapPowerDown(void) {

    EEPROM_CS = 0;
    SPI_RW(DPD);
    EEPROM_CS = 1;
    return 0;

}

BYTE ReleaseFromDeapPowerDown(void) {

    BYTE ES;
    EEPROM_CS = 0;
    SPI_RW(RDID);
    SPI_RW(0);
    SPI_RW(0);
    ES = SPI_RW(0);
    EEPROM_CS = 1;

    if (ES == 0x29)
        return 0;
    else
        return 1;
}

BYTE getCRC(const BYTE* data, const int width) {

    BYTE crc, in0, in1, in2;
    int i, j;
    crc = 0x41;

    for (j = 0; j < width; j++) {
        for (i = 0; i < 8; i++) {
            in0 = ((data[j] >> (7 - i)) & 0x01) ^ ((crc >> 7) & 0x01);
            in1 = in0 ^ (crc & 0x01);
            in2 = in0 ^ ((crc >> 1) & 0x01);
            crc = in0 | (in1 << 1) | (in2 << 2) | ((crc << 1) & 0xF8);
        }
    }
    return crc;
}

void DelayMs_SPI(WORD ms) {
    int i, j, k;
    for (i = 0; i < 80; i++) {
        for (j = 0; j < 10; j++) {
            for (k = 0; k < ms; k++) {
                k = k;
            }
        }
    }
}




